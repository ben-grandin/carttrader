import { PrismaClient } from '@prisma/client'

const prisma = new PrismaClient()


export default defineEventHandler(async (event) => {
	if (!event.context.params?.messageId) {
		throw createError({
			statusCode: 400,
			statusMessage: 'messageId is missing',
		});
	}

	const { messageId } = event.context.params

	return await prisma.message.delete({
		where: {
			id: parseInt(messageId),
		},
	})
})
