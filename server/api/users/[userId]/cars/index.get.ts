import { PrismaClient } from '@prisma/client'
import ICar from "~/models/ICar"

const prisma = new PrismaClient()

export default defineEventHandler(async (event) => {
	if (!event.context.params?.userId) {
		throw createError({
			statusCode: 400,
			statusMessage: 'userId is missing',
		});
	}

	const { userId } = event.context.params

	return await prisma.car.findMany({
		where: {
			userId: userId,
		},
		select: {
			id: true,
			name: true,
			image: true,
			price: true,
			description: true,
			city: true,
		} as Record<keyof ICar, true>,
	})
})
