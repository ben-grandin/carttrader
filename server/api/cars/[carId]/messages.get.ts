import { PrismaClient } from '@prisma/client'

const prisma = new PrismaClient()


export default defineEventHandler(async (event) => {
	if (!event.context.params?.carId) {
		throw createError({
			statusCode: 400,
			statusMessage: 'carId is missing',
		});
	}

	const { carId } = event.context.params

	return await prisma.message.findMany({
		where: {
			carId: parseInt(carId),
		},
	})
})
