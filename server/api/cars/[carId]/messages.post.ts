import Joi from 'joi'
import { PrismaClient } from '@prisma/client'

const prisma = new PrismaClient()

const schema = Joi.object({
	email: Joi.string().email().required(),
	phone: Joi.string().length(10).pattern(/^[0-9]+$/).required(),
	name: Joi.string().required(),
	message: Joi.string().required(),
})

export interface ICreateMessage {
	email: string
	phone: string
	name: string
	message: string
}

export default defineEventHandler(async (event) => {
	if (!event.context.params?.carId) {
		throw createError({
			statusCode: 400,
			statusMessage: 'carId is missing',
		});
	}

	const body = await readBody(event)


	const { error, value } = schema.validate(body)

	if (error) {
		throw createError({
			statusCode: 412,
			statusMessage: 'Invalid input',
			message: error.details[0].message,
		})
	}

	const message = await prisma.message.create({
		data: {
			...value,
			email: value.email.toLowerCase(),
			carId: parseInt(event.context.params.carId),
		},
	})

	return {
		data: message,
	}
})

