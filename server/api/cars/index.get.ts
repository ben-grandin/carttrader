import { PrismaClient } from '@prisma/client'

const prisma = new PrismaClient()


export interface ICarFilters {
	city?: string
	make?: string
	minPrice?: string
	maxPrice?: string
}

interface IPrismaCarFilters {
	city?: {
		contains?: string
	}
	make?: {
		contains?: string
	}
	price?: {
		gte?: number
		lte?: number
	}
}

export default defineEventHandler((event) => {
	const { city, make, minPrice, maxPrice }: ICarFilters = getQuery(event) ?? {}

	const filters: IPrismaCarFilters = {}

	if (city) {
		filters.city = {
			contains: city.toLowerCase(),
		}
	}

	if (make) {
		filters.make = {
			contains: make.toLowerCase(),
		}
	}

	if (minPrice || maxPrice) {
		filters.price = {}
		if (minPrice) {
			filters.price.gte = Math.min(parseInt(minPrice), 2147483647)
		}
		if (maxPrice) {
			filters.price.lte = Math.min(parseInt(maxPrice), 2147483647)
		}
	}

	return prisma.car.findMany({
		where: filters,
	})
})
