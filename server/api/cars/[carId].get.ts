import { PrismaClient } from '@prisma/client'

const prisma = new PrismaClient()

export default defineEventHandler(async (event) => {
	if (!event.context.params?.carId) {
		throw createError({
			statusCode: 400,
			statusMessage: 'carId is missing',
		});
	}

	return await prisma.car.findUnique({
		where: {
			id: parseInt(event.context.params!.carId),
		},
	});
})
