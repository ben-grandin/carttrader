import { PrismaClient } from '@prisma/client'

const prisma = new PrismaClient()

export default defineEventHandler(async (event) => {
		if (!event.context.params?.carId) {
			throw createError({
				statusCode: 400,
				statusMessage: 'carId is missing',
			});
		}

		const { carId } = event.context.params

		try {
			await prisma.car.delete({
				where: {
					id: parseInt(carId),
				},
			})

			setResponseStatus(event, 204)
		} catch (e: unknown) {
			const error = e as Error
			console.error(error)

			if (error.message.toLowerCase().includes('not found')) {
				throw createError({
					statusCode: 404,
					statusMessage: 'Car not found',
				})
			}

			throw createError({
				statusCode: 500,
				statusMessage: 'Unable to delete car',
			})
		}
	},
)
