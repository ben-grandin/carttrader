import Joi from 'joi'
import { PrismaClient } from '@prisma/client'
import ICar from "~/models/ICar"

const prisma = new PrismaClient()

const schema = Joi.object({
	name: Joi.string().required(),
	year: Joi.number().min(1886).max(new Date().getFullYear() + 1).required(),
	price: Joi.number().required(),
	model: Joi.string().required(),
	seats: Joi.number().required(),
	make: Joi.string().required(),
	city: Joi.string().min(2).required(),
	miles: Joi.number().min(0).required(),
	features: Joi.array().items(Joi.string()).required(),
	description: Joi.string().required(),
	image: Joi.required(),// ToDo
	userId: Joi.string().required(),
})
export type  ICreateCarWithUser = Omit<ICar, 'id'> & {
	userId: string
}

export default defineEventHandler(async (event) => {
	const body = await readBody(event)

	if (body.features && typeof body.features === 'string') {
		body.features = body.features.split(',')
	}

	const { error, value } = schema.validate(body)

	if (error) {
		console.error(error)
		throw createError({
			statusCode: 412,
			statusMessage: error.details[0].message,
		})
	}
	const car = await prisma.car.create({
		data: {
			...value,
			image: value.image,
			city: value.city.toLowerCase(),
		},
	})

	return {
		data: car,
	}
})
