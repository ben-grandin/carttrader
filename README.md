# CarTrader

The project is a car trading platform where users can buy and sell cars.

This a portfolio project that I am working on to showcase my skills a full-stack developer.  
The project is built using **Nuxt 3**, **Typescript**  and **Tailwind CSS**.

Here the demo of the project: [CarTrader](https://carttrader.vercel.app/)

## Technos

<p>
<a href="https://www.typescriptlang.org/" target="_blank" rel="noreferrer">
<img src="https://raw.githubusercontent.com/danielcranney/readme-generator/main/public/icons/skills/typescript-colored.svg" width="36" height="36" alt="TypeScript" />
</a>
<a href="https://vuejs.org/" target="_blank" rel="noreferrer">
<img src="https://raw.githubusercontent.com/danielcranney/readme-generator/main/public/icons/skills/vuejs-colored.svg" width="36" height="36" alt="Vue" />
</a>
<a href="https://nuxtjs.org/" target="_blank" rel="noreferrer">
<img src="https://raw.githubusercontent.com/danielcranney/readme-generator/main/public/icons/skills/nuxtjs-colored.svg" width="36" height="36" alt="Nuxtjs" />
</a>
<a href="https://tailwindcss.com/" target="_blank" rel="noreferrer">
<img src="https://raw.githubusercontent.com/danielcranney/readme-generator/main/public/icons/skills/tailwindcss-colored.svg" width="36" height="36" alt="TailwindCSS" />
</a>
</p>

## Features

### Current

- **Authentication**: Users can sign up and login to the platform with google using supabase.
- **CRUD Operations**: Users can create, read, and delete their car listings.
- **Image Upload**: Users can upload images of their cars.
- **Search**: Users can search for cars by city, make, and price.
- **Responsive**: The application is responsive and works on all devices.
- **Toast Notifications**: The application has toast notifications.
- **Error Handling**: The application has error handling.

### Upcoming Features

- **Loading Spinner**: The application has a loading spinner.
- **Pinia**: Will be use modal management.
- **Pagination**: Users can paginate through the car listings.
- **Update Car Listing**: Users can update their car listings.

## Setup

The development server on `http://localhost:3000`:

```bash
pnpm install

pnpm run dev
```

## Production

Build the application for production:

```bash
# npm
pnpm run build
```

Locally preview production build:

```bash
pnpm run preview
```

Check out the [deployment documentation](https://nuxt.com/docs/getting-started/deployment) for more information.
