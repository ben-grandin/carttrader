import { useToastsStore } from '#imports'

export default defineNuxtPlugin((nuxtApp) => {
	nuxtApp.vueApp.config.errorHandler = (err, instance, info) => {
		console.info('----- Error handler', err, instance, info)
		const { errorToast } = useToastsStore()
		const error = err as Error
		errorToast(error.message)
	}
})
