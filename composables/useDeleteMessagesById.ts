import IMessage from '~/models/IMessage'

export default async function useDeleteMessagesById(id: string) {
	const { data, error, refresh } = await useFetch<IMessage[]>(`/api/messages/${ id }`, {
		method: 'DELETE',
	})

	if (error.value) {
		throw createError({
			...error.value,
			statusMessage: error.value.statusMessage ?? 'Unable to delete message',
		})
	}

	return {
		data: data as globalThis.Ref<IMessage[]>,
		refresh,
	}
}

