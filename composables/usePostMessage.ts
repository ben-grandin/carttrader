import IMessage from '~/models/IMessage'
import { ICreateMessage } from '~/server/api/cars/[carId]/messages.post'

export default async function usePostMessage(carId: string, message: ICreateMessage) {
	const { data, error } = await useFetch<IMessage>(`/api/cars/${ carId }/messages`, {
		method: "POST",
		body: message,
	})

	if (error.value) {
		throw createError({
			...error.value,
			statusMessage: error.value.statusMessage ?? 'Unable to post message',
		})
	}

	return {
		data: data as globalThis.Ref<IMessage>,
	}
}
