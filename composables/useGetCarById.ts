import ICar from '~/models/ICar'

export default async function useGetCarById(id: string) {
	const { data, error, refresh } = await useFetch<ICar>(`/api/cars/${ id }`)

	if (error.value) {
		throw createError({
			...error.value,
			statusMessage: error.value.statusMessage ?? 'Unable to fetch car',
		})
	}

	return {
		data: data as globalThis.Ref<ICar>,
		refresh,
	}
}

