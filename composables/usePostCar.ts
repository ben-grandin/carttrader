import ICar from "~/models/ICar"

import { ICreateCarWithUser } from '~/server/api/cars/index.post'

export default async function usePostCar(car: ICreateCarWithUser) {
	const { data, error } = await useFetch<ICar>("/api/cars", {
		method: "POST",
		body: car,
	});

	if (error.value) {
		throw createError({
			...error.value,
			statusMessage: error.value.statusMessage ?? "Unable to create car",
		});
	}

	return {
		data: data as globalThis.Ref<ICar>,
	};
}
