import { ICarFilters } from '~/server/api/cars/index.get'
import ICar from '~/models/ICar'

export default async function useGetCarsByCity(filters?: ICarFilters): Promise<{ data: ICar[] }> {
	if (!filters) throw new Error('Filters is required')

	try {
		// useFetch trigger warning because it's with user interaction #30
		const data = await $fetch<ICar[]>('/api/cars/', { params: filters })
		return { data }
	} catch (error) {
		throw createError({
			...error as Error,
			statusMessage: 'Unable to fetch cars',
		})

	}
}
