export const useAuth = () => {
	const supabase = useSupabaseClient();
	const user = useSupabaseUser();

	const runtimeConfig = useRuntimeConfig()
	const login = async (): Promise<void> => {
		const { error } = await supabase.auth.signInWithOAuth({
			provider: 'google',
			options: {
				redirectTo: `${ runtimeConfig.app.cdnURL }/confirm`,
			},
		});

		if (error) {
			throw createError({
				...error,
				statusCode: error.status ?? 500,
				statusMessage: error.message ?? 'Error logging in with google',
			})
		}
	};

	const logout = async () => {
		const { error } = await supabase.auth.signOut()
		if (error) {
			console.error('Error logging out', error)
		} else {
			navigateTo('/')
		}
	}

	return {
		user,
		login,
		logout,
	};
};
