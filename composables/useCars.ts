import makes from '@/data/makes.json'

interface IUseCars {
	makes: string[]
}

export const useCars = (): IUseCars => {
	return {
		makes,
	}
}
