export default async function useCarsDelete(id: string) {
	const { error } = await useFetch(`/api/cars/${ id }`, {
		method: 'DELETE',
	})

	if (error.value) {
		throw createError({
			...error.value,
			statusMessage: error.value.statusMessage,
		})
	}

	return
}

