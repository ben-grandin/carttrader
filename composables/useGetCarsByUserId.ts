import ICar from "~/models/ICar"

export default async function useGetCarsByUserId(userId: number|string) {
	const { data, error, refresh } = useFetch<ICar[]>(`/api/users/${ userId }/cars`)

	if (error.value) {
		throw createError({
			...error.value,
			statusMessage: error.value.statusMessage ?? 'Unable to fetch car',
		})
	}

	return {
		data: data as globalThis.Ref<ICar[]>,
		refresh,
	}
}
