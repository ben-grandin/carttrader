export enum ToastStatus {
	SUCCESS = 'success',
	ERROR = 'error',
	WARNING = 'warning',
	INFO = 'info',
}

export interface IToast {
	id: string
	text: string
	status: ToastStatus
}


export const useToastsStore = defineStore('toasts', () => {
	const toasts = ref<IToast[]>([])

	function createToast(text: string, timeout = 3000, status = ToastStatus.SUCCESS) {
		const id = Math.random().toString(36).substring(2, 9)
		const toast: IToast = { id, text, status }

		toasts.value.push(toast)

		setTimeout(() => {
			const index = toasts.value.findIndex((t) => t.id === id)
			if (index !== -1) toasts.value.splice(index, 1)
		}, timeout)
	}

	function successToast(text: string, timeout = 3000) {
		createToast(text, timeout, ToastStatus.SUCCESS)
	}

	function errorToast(text: string, timeout = 3000) {
		createToast(text, timeout, ToastStatus.ERROR)
	}

	function warningToast(text: string, timeout = 3000) {
		createToast(text, timeout, ToastStatus.WARNING)
	}

	function infoToast(text: string, timeout = 3000) {
		createToast(text, timeout, ToastStatus.INFO)
	}

	function deleteToast(id: string) {
		const index = toasts.value.findIndex((t) => t.id === id)
		if (index !== -1) toasts.value.splice(index, 1)
	}

	return {
		toasts,
		createToast,
		deleteToast,
		successToast,
		errorToast,
		warningToast,
		infoToast,
	}
})
