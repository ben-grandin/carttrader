export type MODAL_ENUM = 'autocomplete'

export const useModalsStore = defineStore('modals', () => {
	const modals = ref<Record<MODAL_ENUM, boolean>>({
		autocomplete: false,
	})

	function openModal(id: MODAL_ENUM) {
		if (!modals.value[id]) modals.value[id] = true
	}

	function closeModal(id: MODAL_ENUM) {
		if (modals.value[id]) modals.value[id] = false
	}

	function toggleModal(id: MODAL_ENUM) {
		modals.value[id] = !modals.value[id]
	}

	function resetModals() {
		Object.keys(modals.value).forEach(key => {
			modals.value[key as MODAL_ENUM] = false;
		});
	}

	return {
		modals: modals.value,
		openModal,
		closeModal,
		toggleModal,
		resetModals,
	}
})
