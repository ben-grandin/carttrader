// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
	typescript: {
		typeCheck: true,
		strict: true,
	},
	devtools: { enabled: true },
	modules: [
		'nuxt-icon',
		'@nuxtjs/tailwindcss',
		'@nuxt/image',
		'@vueuse/nuxt',
		'@nuxtjs/supabase',
		'@pinia/nuxt',
	],
	supabase: {
		redirectOptions: {
			login: '/login',
			callback: '/confirm',
			include: undefined,
			exclude: [ '/login', '/' ],
			cookieRedirect: false,
		},
	},
})
