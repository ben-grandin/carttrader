export default interface IMessage {
	id: number;
	name: string;
	email: string;
	phone: string;
	text: string;
	carId: number;
}
