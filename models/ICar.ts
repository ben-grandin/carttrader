export default interface ICar {
	id: number
	name: string
	price: number
	model: string
	image: string
	seats: number
	miles: string
	features: string[]
	description: string
	year: number
	make: string
	city: string
}
