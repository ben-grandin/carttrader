import type { JestConfigWithTsJest } from 'ts-jest'

const config: JestConfigWithTsJest = {
	preset: 'ts-jest',
}

module.exports = config;
