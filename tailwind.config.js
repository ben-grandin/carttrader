/** @type {import("tailwindcss").Config} */
export default {
	content: [
		"./components/**/*.{vue,js,ts,jsx,tsx}",
		"./layouts/**/*.vue",
		"./pages/**/*.vue",
		"./*.vue",
		"./plugins/**/*.{js,ts}",
	],
	theme: {
		extend: {
			screens: {
				"xs": "480px",
			},
			width: {
				"inherit": "inherit",
			},
		},
	},
	plugins: [],
}
