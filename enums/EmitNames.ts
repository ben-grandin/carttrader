export enum E_EMIT_NAMES {
	UPDATE_VALUE = 'update:value',
	updateModelValue = 'update:modelValue',
	deleteClick = 'deleteClick'
}
