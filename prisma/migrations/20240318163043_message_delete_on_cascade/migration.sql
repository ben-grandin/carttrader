-- DropForeignKey
ALTER TABLE "Message" DROP CONSTRAINT "Message_carId_fkey";

-- AddForeignKey
ALTER TABLE "Message" ADD CONSTRAINT "Message_carId_fkey" FOREIGN KEY ("carId") REFERENCES "Car"("id") ON DELETE CASCADE ON UPDATE CASCADE;
