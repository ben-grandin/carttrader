import { RouteLocationNormalizedLoaded } from 'vue-router'
import { getFirstArgIfArray } from '~/utils/getFirstArgIfArray'

export default function getRouteQuery(route: RouteLocationNormalizedLoaded, key: string): string | undefined {
	return getFirstArgIfArray(route.query[key], `Multiple values found for param key: ${ key }`)
}
