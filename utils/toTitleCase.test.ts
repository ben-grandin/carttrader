import toTitleCase from './toTitleCase'

describe("toTitleCar", () => {
	it("should return a single word in title case", () => {
		expect(toTitleCase("hello")).toBe("Hello");
	});

	it("should return a sentence in title case", () => {
		expect(toTitleCase("hello world")).toBe("Hello World");
	});

	it("should return an array of words in title case", () => {
		expect(toTitleCase([ "hello", "world" ])).toBe("Hello World");
	});

	it("should handle empty string", () => {
		expect(toTitleCase("")).toBe("");
	});

	it("should handle empty array", () => {
		expect(toTitleCase([])).toBe("");
	});

	it("should handle array with empty string", () => {
		expect(toTitleCase([ "" ])).toBe("");
	});
});
