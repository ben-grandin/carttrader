export default function parseIntWithUndefined(value: string | undefined): number | undefined {
	return value ? parseInt(value) : undefined
}
