// Function overloads
export function getFirstArgIfArray(arg: string | string[], warningMessage?: string): string;
export function getFirstArgIfArray(arg: string | null | Array<string | null>, warningMessage?: string): string | undefined;

// Implementation
export function getFirstArgIfArray(arg: string | null | Array<string | null>, warningMessage?: string): string | undefined {
	if (Array.isArray(arg)) {
		if (warningMessage) console.warn(warningMessage);
		if (arg.length === 0) return undefined; // Handle the case where the array is empty
		const firstElement = arg[0];
		return firstElement === null ? undefined : firstElement;
	} else {
		return arg === null ? undefined : arg;
	}
}
