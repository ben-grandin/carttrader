export default function getImageUrl(imagePath: string) {
	const isSupabaseUrl = !imagePath.includes('http')

	if (!isSupabaseUrl) return imagePath

	const supabase = useSupabaseClient()
	const { data } = supabase
	.storage
	.from('cartrader')
	.getPublicUrl(imagePath)

	// getPublicUrl return wrong url
	return data.publicUrl.replace('/cartrader/', '/images/')
}
