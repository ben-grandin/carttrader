const displayCurrency = (number?: number | null | string) => {
	if (!number) return ''
	if (typeof number === 'string') number = parseFloat(number)


	return new Intl.NumberFormat('en-EN', {
		style: 'currency',
		currency: 'USD',
		minimumFractionDigits: number % 1 === 0 ? 0 : 2,
	}).format(number)
}
export default displayCurrency
