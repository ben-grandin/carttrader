export default function toTitleCase(str: string | string[]): string {
	if (Array.isArray(str)) {
		return str.map(toTitleCase).join(' ')
	}

	return str.replace(/\w\S*/g, function(txt: string) {
		return txt.charAt(0).toUpperCase() + txt.substring(1)
	})
}
