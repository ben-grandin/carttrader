import { Ref } from 'vue'

export default function emptyObjectRef(form: Ref) {
	Object.keys(form.value).forEach((key) => {
		form.value[key] = undefined
	})
}
