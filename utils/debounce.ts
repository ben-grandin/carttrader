export default function debounce<T extends (...args: any[]) => any>(
	func: T,
	wait: number = 300,
): (...funcArgs: Parameters<T>) => void {

	let timeout: NodeJS.Timeout

	return function(this: ThisParameterType<T>, ...args: any[]): void {
		const context = this
		clearTimeout(timeout)
		timeout = setTimeout(() => func.apply(context, args), wait)
	}
}
