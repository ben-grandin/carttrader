import { RouteLocationNormalizedLoaded } from 'vue-router'
import { getFirstArgIfArray } from '~/utils/getFirstArgIfArray'

export default function getRouteParams(route: RouteLocationNormalizedLoaded, key: string): string {
	return getFirstArgIfArray(route.params[key], `Multiple values found for param key: ${ key }`)
}
